// JS
import "../build/tags.js";
import "./components/app.js";
import "./components/router.js";

// JS Libraries
import riot from "riot";

riot.mount("#view", "app");
