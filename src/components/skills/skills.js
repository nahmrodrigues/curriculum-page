import "./skills.scss";
import riot from "riot";

const skills = {
    'technologies': [
        {
            'technology': "Python",
            'level': "Intermediário"
        },
        {
            'technology': "Java",
            'level': "Intermediário"
        },
        {
            'technology': "JavaScript",
            'level': "Intermediário"
        },
        {
            'technology': "HTML/CSS",
            'level': "Intermediário"
        },
        {
            'technology': "C/C++",
            'level': "Avançado"
        },
        {
            'technology': "SQL",
            'level': "Intermediário"
        },
        {
            'technology': "Django",
            'level': "Avançado"
        },
        {
            'technology': "Django Rest Framework",
            'level': "Intermediário"
        },
        {
            'technology': "Riot.js",
            'level': "Intermediário"
        },
        {
            'technology': "Angular",
            'level': "Básico"
        }
    ],
    'tools': [
        {
            'tool': "Git",
            'level': "Avançado"
        },
        {
            'tool': "Eclipse",
            'level': "Intermediário"
        },
        {
            'tool': "VS Code",
            'level': "Intermediário"
        },
        {
            'tool': "Excel",
            'level': "Intermediário"
        },
        {
            'tool': "Word",
            'level': "Intermediário"
        },
        {
            'tool': "Power Point",
            'level': "Intermediário"
        },
    ],
    'languages': [
        {
            'language': "Português",
            'level': "Nativo"
        },
        {
            'language': "Inglês",
            'level': "Intermediário"
        },
        {
            'language': "Espanhol",
            'level': "Básico"
        }
    ]
};

riot.mixin("skills-list", {skills});