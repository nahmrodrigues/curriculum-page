import "./formation.scss";
import riot from "riot";


const formations = [
    {
        'description': "Graduanda em Engenharia de Software",
        'location' : "UnB - Universidade de Brasília",
        'extraInformation' : "Cursando o 8º semestre.",
        'period' : "mar/2016 - ago/2021"
    }
]

riot.mixin("formations-list", {formations});