// routing
import riot from "riot";
import route from "riot-route";

route.base("#/");
route.start(true);

route("/", function() {
    riot.mount("#content", "about");
});

route("/experience", function() {
    riot.mount("#content", "experience");
})

route("/formation", function() {
    riot.mount("#content", "formation");
})

route("/skills", function() {
    riot.mount("#content", "skills");
})