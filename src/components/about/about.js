import "./about.scss";
import riot from "riot";

const aboutText = "Sou estudante de Engenharia de Software na Universidade de Brasília. " +
                  "Meu foco é aprender e me manter atualizada no que se diz respeito às novas ferramentas, " +
                  "tecnologias e metodologias de desenvolvimento. Tenho como objetivo principal ser uma " + 
                  "uma profissional competente e comprometida com o sucesso dos projetos dos quais participo.";


riot.mixin("about-text", {aboutText});
