// importing components / tags
import "./about/about";
import "./experience/experience";
import "./formation/formation";
import "./nav-bar/nav-bar";
import "./side-bar/side-bar";
import "./skills/skills"


// importing styling files
import "./app.scss";