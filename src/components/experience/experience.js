import "./experience.scss";
import riot from "riot";

const experiences = [
    {
        'role': "Estagiária",
        'location': "Cabal Brasil",
        'description': "Apoio nas atividades de desenvolvimento.",
        'period': "agosto/2019 - presente"
    },
    {
        'role': "Estagiária",
        'location': "Tribunal Regional Federal da Primeira Região",
        'description': "Apoio nas atividades de gerenciamento de redes, gestão VoIP, " +
                       "administração da Rede Departamental e atendimento a usuários do TRF1.",
        'period': "jan/2019 - jul/2019"
    },
    {
        'role': "Bolsista",
        'location': "Information Technology Research and Application Center - UnB",
        'description': "Projeto de Pesquisa, Desenvolvimento e Inovação para a Automação de Serviços" +
                       "Públicos, fomentado pelo Ministério de Planejamento em parceria com a" +
                       "Universidade de Brasília.\n" +
                       "Apoio nas atividades de desenvolvimento e testes (manuais e automatizados).",
        'period': "fev/2018 - jan/2019"
    },
]

riot.mixin("experiences-list", {experiences});