import './side-bar.scss';
import riot from "riot";

import profile from "../../img/profile.jpg";

riot.mixin("profile-image", {profile});