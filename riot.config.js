const path = require("path"); // native node package

module.exports = {
    from: "./src",
    ext: "tag.html",
    to: path.resolve(__dirname, "build") + "/tags.js"
};

// __dirname is a node variable with the path of the actual folder
